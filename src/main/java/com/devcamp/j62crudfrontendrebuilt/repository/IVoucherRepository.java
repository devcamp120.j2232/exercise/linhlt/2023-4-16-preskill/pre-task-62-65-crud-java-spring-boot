package com.devcamp.j62crudfrontendrebuilt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j62crudfrontendrebuilt.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
