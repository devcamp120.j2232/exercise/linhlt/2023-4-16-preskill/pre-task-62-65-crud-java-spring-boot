package com.devcamp.j62crudfrontendrebuilt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J62CrudFrontEndRebuiltApplication {

	public static void main(String[] args) {
		SpringApplication.run(J62CrudFrontEndRebuiltApplication.class, args);
	}

}
